var aurigma_iufield_upload_size = 0;
function aurigma_iufield_afterUpload(html) {
	var u = getImageUploader('ImageUploader');
	var max_file_size = 0 + u.getMaxTotalFileSize();
	if (max_file_size > 1) {
		max_file_size -= aurigma_iufield_upload_size;
		if (max_file_size < 1) {
			max_file_size = 1;
		}
		u.setMaxTotalFileSize(max_file_size);
	}
	var response = Drupal.parseJson(html);
	var wrapper = $("#" + aurigma_iufield_field_name);
	// Manually insert HTML into the jQuery object, using $() directly crashes
	// Safari with long string lengths. http://dev.jquery.com/ticket/1152
	//var new_content = $('<div></div>').html(response.data);

	// Add the new content to the page.
	Drupal.freezeHeight();
	wrapper.replaceWith(response.data);

	// Attach all javascript behaviors to the new content, if it was
	// successfully
	// added to the page
	//if (new_content.parents('html').length > 0) {
	//	Drupal.attachBehaviors(new_content);
	//}
	Drupal.attachBehaviors($("#" + aurigma_iufield_field_name));

	Drupal.unfreezeHeight();

	if (submitButton != null) {
		setTimeout(function() {
					$(submitButton).click();
				}, 100);
	}
}

function aurigma_iufield_beforeUpload() {
	var u = getImageUploader('ImageUploader');
	aurigma_iufield_upload_size = u.getTotalFileSize();
}

var submitButton = null;
$(function() {
  var form_id = 'node-form';
	$('.form-submit').click(function(ev) {
    var u = getImageUploader('ImageUploader');
    if (u != null && u.getUploadFileCount() > 0) {
      submitButton = this;
      u.Send();
      ev.preventDefault();
    }
  });
});