<?php
/**
 * @file
 * The Aurigma admin settings form.
 *
 * The form provides control of the module as well as the Aurigma applet itself.
 */

function aurigma_iufield_admin_settings() {

  $form['image_uploader'] = array(
    '#type' => 'fieldset',
    '#weight' => $group_weight,
    '#title' => t('Image Uploader Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['image_uploader']['aurigma_iufield_licensekey'] = array(
    '#type' => 'textfield',
    '#size' => '80',
    '#title' => t('License Key'),
    '#default_value' => variable_get('aurigma_iufield_licensekey', ''),
    '#description' => t('You need to have a valid key to use the upload solution. Enter both Java and ActiveX keys together.<br />'.
                        'For example: 71060-xxxxx-00000-xxxxx-xxxxx;72060-xxxxx-00000-xxxxx-xxxxx.'),
    '#required' => TRUE,
  );

  $form['image_uploader']['aurigma_iufield_uploader_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#default_value' => variable_get('aurigma_iufield_uploader_size', '100%x400px'),
    '#size' => 15,
    '#maxlength' => 15,
    '#description' => t('The size (e.g. 650x400) of Image Uploader.'),
    '#field_suffix' => '<kbd>'. t('WIDTHxHEIGHT') .'</kbd>',
    '#element_validate' => array('_aurigma_iufield_uploader_size_validate'),
  );

  return system_settings_form($form);
}

function _aurigma_iufield_uploader_size_validate($element, &$form_state) {
  if ($element['#value']) {
    if (!preg_match('#^([0-9]+(%|px)?)x([0-9]+(%|px)?)$#', $element['#value'])) {
      form_error($element, t('Invalid size format.'));
    }
  } else {
    form_set_value($element, '100%x400px', $form_state);
  }
}