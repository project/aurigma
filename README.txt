
/**
 * @file
 * README.txt file for Aurigma Mass Uploader for CCK module.
 */

Description
The Aurigma Mass Uploader for CCK module provides Image Uploader widget for FileFied/ImageField modules. Multiple images are uploaded in a single batch into a multi-values field.

Requirements
 - Modules: CCK, filefield, imagefield
 - Aurigma Image Uploader is a commercial product. A valid license key is required.

Benefits
 - Allows batch image uploading to imagefield.
 - Set image descriptions in the uploader and/or the imagefield.
 - Deletions are handled by imagefield and filefield.
 - Client-side validation for file type and maximum file size.

Installation
 - See INSTALL.txt

Credits
 - Major thanks go to Andrew Simontsev (http://drupal.org/user/458356) of Aurigma for adding ajax image loading!
 - Author: awolfey http://drupal.org/user/277371