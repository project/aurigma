<?php

/**
 * @file
 * The aurigma uploader sends images to this file, which is a menu callback.
 */

// import content_field_form function
module_load_include('inc', 'content', 'includes/content.node_form');

function aurigma_iufield_upload($type_name, $field_name) {
  // If no $_FILES or $_POST we are here by mistake. Send to home page.
  if (!$_FILES || !$_POST) {
    drupal_goto();
  }
  _aurigma_iufield_change_post($type_name, $field_name);

  // Add new field items and build the form
  $form = _aurigma_iufield_add_new_fields($type_name, $field_name);

  // Render the new output.
  
  $type = content_types($type_name);
  // Special case if fieldgroup module enabled
  if (module_exists('fieldgroup') && ($group_name = _fieldgroup_field_get_group($type['type'], $field_name))) {
    $field_form = $form[$group_name][$field_name];
  }
  else {
    $field_form = $form[$field_name];
  }

  // If a newly inserted widget contains AHAH behaviors, they normally won't
  // work because AHAH doesn't know about those - it just attaches to the exact
  // form elements that were initially specified in the Drupal.settings object.
  // The new ones didn't exist then, so we need to update Drupal.settings
  // by ourselves in order to let AHAH know about those new form elements.
  $javascript = drupal_add_js(NULL, NULL);
  $output_js = isset($javascript['setting']) ? '<script type="text/javascript">jQuery.extend(Drupal.settings, '.
  drupal_to_js(call_user_func_array('array_merge_recursive', $javascript['setting'])) .');</script>' : '';

  $output = theme('status_messages') . drupal_render($field_form) . $output_js;

  $GLOBALS['devel_shutdown'] =  FALSE;
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * This function based on the content_add_more_js function,  which adds new
 * items to the field.
 *
 * @param $type_name
 * @param $field_name
 * @return unknown_type
 */
function _aurigma_iufield_add_new_fields($type_name, $field_name) {
  $type = content_types($type_name);
  $field = content_fields($field_name, $type_name);

  if (($field['multiple'] != 1) || empty($_POST['form_build_id'])) {
    // Invalid request.
    drupal_json(array('data' => ''));
    exit;
  }

  // Retrieve the cached form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  if (!$form) {
    // Invalid form_build_id.
    drupal_json(array('data' => ''));
    exit;
  }

  // Build our new form element for the whole field
  // In _aurigma_iufield_change_post we already add new elements into the $_POST[$field_name]
  $form_state['item_count'] = array($field_name => count($_POST[$field_name]));
  $form_element = content_field_form($form, $form_state, $field);

  // Let other modules alter it.
  drupal_alter('form', $form_element, array(), 'content_add_more_js');

  // Add the new element at the right place in the (original, unbuilt) form.
  if (module_exists('fieldgroup') && ($group_name = _fieldgroup_field_get_group($type['type'], $field_name))) {
    $form[$group_name][$field_name] = $form_element[$field_name];
  }
  else {
    $form[$field_name] = $form_element[$field_name];
  }

  // Save the new definition of the form.
  $form_state['values'] = array();
  form_set_cache($form_build_id, $form, $form_state);

  // The form that we get from the cache is unbuilt. We need to build it so that
  // _value callbacks can be executed and $form_state['values'] populated.
  // We only want to affect $form_state['values'], not the $form itself
  // (built forms aren't supposed to enter the cache) nor the rest of $form_data,
  // so we use copies of $form and $form_data.
  $form_copy = $form;
  $form_state_copy = $form_state;
  $form_copy['#post'] = array();
  form_builder($_POST['form_id'], $form_copy, $form_state_copy);
  // Just grab the data we need.
  $form_state['values'] = $form_state_copy['values'];
  // Reset cached ids, so that they don't affect the actual form we output.
  form_clean_id(NULL, TRUE);

  // Build the new form against the incoming $_POST values so that we can
  // render the new element.
  $form_state = array('submitted' => FALSE);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );

  // Build the form. This calls the filefield's #value_callback function and
  // saves the uploaded file.
  $form = form_builder($_POST['form_id'], $form, $form_state);
  
  return $form;
}

/**
 * We need to do some changes in $_POST and $_FILES.
 * 1. Drupal post all files to $_FILES['files'] array.
 *    We create the same structure with our posted files.
 * 2. We need to remove posted value for 'filefield_remove' button.
 *    Otherwise filefield thinks the Remove button was pressed and removes
 *    previously uploaded files.
 */
function _aurigma_iufield_change_post($type_name, $field_name) {

  // Count of posted files
  $file_count = $_POST['FileCount'];

  // Max item number for field
  if (isset($_POST[$field_name])) {
    $delta = max(array_keys($_POST[$field_name]));
  } else {
    $delta = -1;
  }


  // Create $_FILES['files'] array. It should contains file posted by filefield.
  if ($file_count > 0) {
    for ($i = 1; $i <= $file_count; $i++) {
      // Field name containig original file
      $posted_file = "SourceFile_$i";

      $upload_name = $field_name .'_'. ($delta + $i);
      // Adapt to drupal files structure
      $_FILES['files']['name'][$upload_name] = $_FILES[$posted_file]['name'];
      $_FILES['files']['type'][$upload_name] = $_FILES[$posted_file]['type'];
      $_FILES['files']['tmp_name'][$upload_name] = $_FILES[$posted_file]['tmp_name'];
      $_FILES['files']['error'][$upload_name] = $_FILES[$posted_file]['error'];
      $_FILES['files']['size'][$upload_name] = $_FILES[$posted_file]['size'];

      // Add new item in field elements
      $_POST[$field_name][$delta + $i]['_weight'] = $delta + $i;
    }
  }

  // We didn't press 'Remove' button, so delete them from $_POST values
  // Otherwise filefield deletes all previously uploaded files while processing
  // the form.
  foreach ($_POST as $key => $value) {
  	if (preg_match('/^field_.+_filefield_remove$/', $key)) {
      unset($_POST[$key]);
    }
  }
}
